// var - устаревший тег переменной. Не используется из-за большого количества багов и неудобств. Использование считается плохим тоном, по причинам, описанным раньше.
// let - используемый сейчас тег переменной. Отличается тем, что в любой момент в коде - можно изменить его наполнение.
// const - используем в случае, если переменная не будет меняться. Для удобства.

let userName = prompt ('Hi! Enter your name here.') // Подскажите, пожалуйста, как сделать чтобы валидным был лишь string?
let userAge = +prompt ('Enter your age here.')
while (!userAge) {
    userAge = +prompt('Enter your real age, please.')
}
const allowedAge = 18
const allowedAge22 = 22
if (userAge < allowedAge) {
    alert ('You are not allowed to visit this website.')
} else if (userAge >= allowedAge && userAge <= allowedAge22) {
    let isHeSure = confirm ('Are you sure you want to continue?')
    if (isHeSure) {
        alert (`Welcome, ${userName}!`)
    } else if (!isHeSure) {
        alert ('You are not allowed to visit this website.')
    }
} else if (userAge > allowedAge22) {
    alert (`Welcome, ${userName}`)
}